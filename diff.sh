#!/bin/bash

set -e
set -u
set -o pipefail
shopt -s inherit_errexit

while (( $# > 0 )); do
    case $1 in
        -j|--jobs)
            n_jobs="$2"
            shift
            ;;
        -h|--help)
            cat << EOF
Usage: check-armhf-time_t [OPTION]... [SOURCE_PACKAGE]...
    -h,     --help      print this message and exit
    -j <n>, --jobs <n>  run 2*n processes in parallel
EOF
            exit 0
            ;;
        # TODO: add an option to skip already-analyzed packages
        *)
            echo "Unknown option $1" >&2
            exit 1
            ;;
    esac
    shift
done

./diff_list_packages_dumped.sh \
    | parallel \
        ${n_jobs+--jobs ${n_jobs}} \
        --memfree 20G \
        --memsuspend 5G \
        --line-buffer \
        ./diff_package.sh

# TODO: call diff_virtuals_reduce.sh
