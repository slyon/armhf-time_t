#!/bin/bash

. ./lib.sh

set -e
set -u
shopt -s inherit_errexit

clean_up() {
    local trap

    local from

    trap="$1"

    if [[ "${trap}" != 'EXIT' ]]; then
        if [[ ${BASH_LINENO[0]} -eq 1 ]]; then
            from="${trap} trap"
            trap - INT TERM ERR EXIT
        else
            from="line ${BASH_LINENO[0]}"
        fi
        log_msg "Clean up (called from ${from})"
    fi
}

# Run a-c-c's diff
a_c_c_diff() {
    local devpkg
    local old
    local new

    devpkg="$1"
    old="$2"
    new="$3"

    if ! abi-compliance-checker -s -l "$devpkg" \
        -filter "diffing/${devpkg}/${devpkg}_base.xml" \
        -old "diffing/${devpkg}/${devpkg}_${old}.dump" \
        -new "diffing/${devpkg}/${devpkg}_${new}.dump" \
        > /dev/null
    then
        test -z "$(grep -E "class='(failed|chg)'" \
            "compat_reports/${devpkg}/${old}_to_${new}/compat_report.html" \
        | grep -v 'Removed Symbols')"
    fi
}

diff() {
    local devpkg

    local lfs='false'
    local time_t='false'

    devpkg="$1"

    # Extract to the diffing directory
    mkdir -p 'diffing'
    tar xf "dumps/${devpkg}.dump.tar.xz" -C 'diffing'

    log_msg "=> ${devpkg}: base=>lfs, lfs=>time_t"

    a_c_c_diff "$devpkg" 'lfs' 'time_t' || time_t='true'
    a_c_c_diff "$devpkg" 'base' 'lfs' || lfs='true'

    analysis_sql_result "${devpkg}" "${lfs}" "${time_t}"
}

trap 'clean_up INT' INT
trap 'clean_up TERM' TERM
trap 'clean_up ERR' ERR
trap 'clean_up EXIT' EXIT

diff "$1"
